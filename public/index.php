<?php
if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

require __DIR__ . '/../vendor/autoload.php';

session_start();

// Instantiate the app
$settings = require __DIR__ . '/../src/settings.php';
$app = new \Slim\App($settings);


$app->get('/test', function (\Slim\Http\Request $request, \Slim\Http\Response $response) {

        $username = "api@playlotto.com";
        $password = "N$33P%2K@fD=6#^X";

    class WsseAuthHeader extends SoapHeader
    {
        private $wss_ns = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
        function __construct($user, $pass, $ns = null)
        {
            if ($ns)
            {
                $this->wss_ns = $ns;
            }
            $auth = new stdClass();
            $auth->Username = new SoapVar($user, XSD_STRING, NULL, $this->wss_ns,
                NULL, $this->wss_ns);
            $auth->Password = new SoapVar($pass, XSD_STRING, NULL, $this->wss_ns,
                NULL, $this->wss_ns);
            $username_token = new stdClass();
            $username_token->UsernameToken = new SoapVar($auth, SOAP_ENC_OBJECT, NULL,
                $this->wss_ns, 'UsernameToken', $this->wss_ns);
            $security_sv = new SoapVar(
                new SoapVar($username_token, SOAP_ENC_OBJECT,
                    NULL, $this->wss_ns, 'UsernameToken', $this->wss_ns),
                SOAP_ENC_OBJECT, NULL, $this->wss_ns, 'Security',
                $this->wss_ns);
            parent::__construct($this->wss_ns, 'Security', $security_sv, true);
        }
    }

        //set header request
        $wsse_header = new WsseAuthHeader($username, $password);
        $options = array(
            'soap_version' => SOAP_1_1,
            'exceptions' => true,
            'trace' => 1,
            'wdsl_local_copy' => true
        );
// This is currently linked to the pilot or test site.
        $wsdl = 'https://pilot.id3global.com/ID3gWS/ID3global.svc?wsdl';
        $soapClient = new SoapClient($wsdl, $options);
        $soapClient->__setSoapHeaders(array($wsse_header));
        $objParam = new stdClass();
        $objParam->InputData = new stdClass();

        $objParam->InputData->Country = 'United Kingdom';

        //send
        if (is_soap_fault($soapClient)) {
            throw new Exception(" {$soapClient->faultcode}: {$soapClient->faultstring} ");
        }
        $objRet = null;
        try {
            $objRet = $soapClient->AddressLookup($objParam);
            echo '<pre>';
                var_dump($objRet);
            echo '</pre>';
        } catch (Exception $e) {
            echo "<pre>";
            print_r($e);
            echo "</pre>";
        }
        if (is_soap_fault($objRet)) {
            throw new Expception(" {$objRet->faultcode}: {$objRet->faultstring} ");
        }


    });

// Set up dependencies
require __DIR__ . '/../src/dependencies.php';

// Register middleware
require __DIR__ . '/../src/middleware.php';

// Register routes
require __DIR__ . '/../src/routes.php';

// Run app
$app->run();
